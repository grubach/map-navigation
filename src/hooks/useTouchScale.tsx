import React from "react";
import { useTransform } from "../contexts/transform";

const MAX_SCALE = 10;
const MIN_SCALE = 0.1;

const useTouchScale = (): null => {
  const [, setTransform] = useTransform();
  const touches = React.useRef<TouchList>();

  const handleTouches = React.useCallback((e: TouchEvent): void => {
    touches.current = e.touches;
  }, []);

  const handleTouchMove = React.useCallback(
    (e: TouchEvent): void => {
      if (e.touches.length < 2) return;

      const first = e.touches[0];
      const firstBefore = touches.current?.[0] || first;

      const second = e.touches[1];
      const secondBefore = touches.current?.[1] || second;

      const section = {
        x: first.pageX - second.pageX,
        y: first.pageY - second.pageY,
      };
      const sectionBefore = {
        x: firstBefore.pageX - secondBefore.pageX,
        y: firstBefore.pageY - secondBefore.pageY,
      };

      const distance = Math.sqrt(
        Math.pow(section.x, 2) + Math.pow(section.y, 2)
      );
      const distanceBefore = Math.sqrt(
        Math.pow(sectionBefore.x, 2) + Math.pow(sectionBefore.y, 2)
      );

      setTransform((transform) => {
        const scale = Math.min(
          Math.max(transform.scale * (distance / distanceBefore), MIN_SCALE),
          MAX_SCALE
        );
        const position = {
          x:
            first.pageX -
            (first.pageX - transform.position.x) * (scale / transform.scale),
          y:
            first.pageY -
            (first.pageY - transform.position.y) * (scale / transform.scale),
        };
        return {
          ...transform,
          position,
          scale,
        };
      });

      touches.current = e.touches;
    },
    [setTransform]
  );

  React.useEffect(() => {
    window.addEventListener("touchstart", handleTouches, false);
    window.addEventListener("touchend", handleTouches, false);

    window.addEventListener("touchmove", handleTouchMove, false);
    return () => {
      window.removeEventListener("touchstart", handleTouches, false);
      window.removeEventListener("touchend", handleTouches, false);

      window.removeEventListener("touchmove", handleTouchMove, false);
    };
  }, [handleTouchMove, handleTouches]);

  return null;
};

export default useTouchScale;
