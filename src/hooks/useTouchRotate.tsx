import React from "react";
import { useTransform } from "../contexts/transform";

const useTouchRotate = (): null => {
  const [, setTransform] = useTransform();
  const touches = React.useRef<TouchList>();

  const handleTouches = React.useCallback((e: TouchEvent): void => {
    touches.current = e.touches;
  }, []);

  const handleTouchMove = React.useCallback(
    (e: TouchEvent): void => {
      if (e.touches.length < 2) return;

      const first = e.touches[0];
      const firstBefore = touches.current?.[0] || first;

      const second = e.touches[1];
      const secondBefore = touches.current?.[1] || second;

      const section = {
        x: first.pageX - second.pageX,
        y: first.pageY - second.pageY,
      };
      const sectionBefore = {
        x: firstBefore.pageX - secondBefore.pageX,
        y: firstBefore.pageY - secondBefore.pageY,
      };

      const angle =
        Math.atan2(section.y, section.x) -
        Math.atan2(sectionBefore.y, sectionBefore.x);

      setTransform((transform) => {
        const rotation = transform.rotation + angle;
        const position = {
          x:
            first.pageX +
            (transform.position.x - first.pageX) * Math.cos(angle) -
            (transform.position.y - first.pageY) * Math.sin(angle),
          y:
            first.pageY +
            (transform.position.x - first.pageX) * Math.sin(angle) +
            (transform.position.y - first.pageY) * Math.cos(angle),
        };
        return {
          ...transform,
          rotation,
          position,
        };
      });

      touches.current = e.touches;
    },
    [setTransform]
  );

  React.useEffect(() => {
    window.addEventListener("touchstart", handleTouches, false);
    window.addEventListener("touchend", handleTouches, false);

    window.addEventListener("touchmove", handleTouchMove, false);
    return () => {
      window.removeEventListener("touchstart", handleTouches, false);
      window.removeEventListener("touchend", handleTouches, false);

      window.removeEventListener("touchmove", handleTouchMove, false);
    };
  }, [handleTouchMove, handleTouches]);

  return null;
};

export default useTouchRotate;
