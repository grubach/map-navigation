import React from "react";
import { useTransform } from "../contexts/transform";

const MAX_SCALE = 10;
const MIN_SCALE = 0.1;
const SCALE_STEP = 0.001;

const useMouseWheel = (): null => {
  const [, setTransform] = useTransform();

  const handleMouseScale = React.useCallback(
    (e: WheelEvent): void => {
      setTransform((transform) => {
        const scale = Math.min(
          Math.max(transform.scale + e.deltaY * SCALE_STEP, MIN_SCALE),
          MAX_SCALE
        );
        const position = {
          x:
            e.pageX -
            (e.pageX - transform.position.x) * (scale / transform.scale),
          y:
            e.pageY -
            (e.pageY - transform.position.y) * (scale / transform.scale),
        };
        return {
          ...transform,
          position,
          scale,
        };
      });
    },
    [setTransform]
  );

  React.useEffect(() => {
    window.addEventListener("wheel", handleMouseScale, false);
    return () => {
      window.removeEventListener("wheel", handleMouseScale, false);
    };
  }, [handleMouseScale]);

  return null;
};

export default useMouseWheel;
