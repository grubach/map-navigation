import React from "react";
import { useTransform } from "../contexts/transform";

const useTouchMove = (): null => {
  const [, setTransform] = useTransform();
  const touches = React.useRef<TouchList>();

  const handleTouches = React.useCallback((e: TouchEvent): void => {
    touches.current = e.touches;
  }, []);

  const handleTouchMove = React.useCallback(
    (e: TouchEvent): void => {
      const anchor = e.touches[0];
      const anchorBefore = touches.current?.[0] || anchor;
      const anchorMove = {
        x: anchor.pageX - anchorBefore.pageX,
        y: anchor.pageY - anchorBefore.pageY,
      };
      setTransform((transform) => {
        const position = {
          x: transform.position.x + anchorMove.x,
          y: transform.position.y + anchorMove.y,
        };
        return {
          ...transform,
          position,
        };
      });
      touches.current = e.touches;
    },
    [setTransform]
  );

  React.useEffect(() => {
    window.addEventListener("touchstart", handleTouches, false);
    window.addEventListener("touchend", handleTouches, false);

    window.addEventListener("touchmove", handleTouchMove, false);
    return () => {
      window.removeEventListener("touchstart", handleTouches, false);
      window.removeEventListener("touchend", handleTouches, false);

      window.removeEventListener("touchmove", handleTouchMove, false);
    };
  }, [handleTouchMove, handleTouches]);

  return null;
};

export default useTouchMove;
