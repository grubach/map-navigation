import React from "react";
import { useTransform } from "../contexts/transform";

const useMouseMove = (): null => {
  const [, setTransform] = useTransform();

  const handleMouseMove = React.useCallback(
    (e: MouseEvent): void => {
      if (e.buttons !== 1) return;

      setTransform((transform) => {
        const position = {
          x: transform.position.x + e.movementX,
          y: transform.position.y + e.movementY,
        };
        return {
          ...transform,
          position,
        };
      });
    },
    [setTransform]
  );

  React.useEffect(() => {
    window.addEventListener("mousemove", handleMouseMove, false);
    return () => {
      window.removeEventListener("mousemove", handleMouseMove, false);
    };
  }, [handleMouseMove]);

  return null;
};

export default useMouseMove;
