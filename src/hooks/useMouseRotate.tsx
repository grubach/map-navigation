import React from "react";
import useWindowSize from "./useWindowSize";
import { useTransform } from "../contexts/transform";

const useMouseRotate = (): null => {
  const windowSize = useWindowSize();
  const [, setTransform] = useTransform();

  const handleMouseMove = React.useCallback(
    (e: MouseEvent): void => {
      if (e.buttons !== 2) return;
      const pivot = { x: windowSize.width / 2, y: windowSize.height / 2 };

      setTransform((transform) => {
        const point = {
          x: e.pageX - pivot.x,
          y: e.pageY - pivot.y,
        };
        const previousPoint = {
          x: e.pageX - e.movementX - pivot.x,
          y: e.pageY - e.movementY - pivot.y,
        };
        const angle =
          Math.atan2(point.y, point.x) -
          Math.atan2(previousPoint.y, previousPoint.x);

        const rotation = transform.rotation + angle;
        const position = {
          x:
            pivot.x +
            (transform.position.x - pivot.x) * Math.cos(angle) -
            (transform.position.y - pivot.y) * Math.sin(angle),
          y:
            pivot.y +
            (transform.position.x - pivot.x) * Math.sin(angle) +
            (transform.position.y - pivot.y) * Math.cos(angle),
        };
        return {
          ...transform,
          position,
          rotation,
        };
      });
    },
    [setTransform, windowSize]
  );

  React.useEffect(() => {
    window.addEventListener("mousemove", handleMouseMove, false);
    return () => {
      window.removeEventListener("mousemove", handleMouseMove, false);
    };
  }, [handleMouseMove]);

  return null;
};

export default useMouseRotate;
