import React from "react";
import { useRobot } from "./contexts/robot";
import "./Form.css";

const Form: React.FC = () => {
  const [robot, setRobot] = useRobot();

  const handleXChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    (e) => {
      setRobot((robot) => ({
        ...robot,
        position: {
          ...robot.position,
          x: parseFloat(e.target.value),
        },
      }));
    },
    [setRobot]
  );

  const handleYChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    (e) => {
      setRobot((robot) => ({
        ...robot,
        position: {
          ...robot.position,
          y: parseFloat(e.target.value),
        },
      }));
    },
    [setRobot]
  );

  return (
    <form className="Form">
      <h3 className="Form__header">Robot position</h3>
      <fieldset className="Form__fieldset">
        <label className="Form__label">
          x<input
            className="Form__input"
            type="number"
            value={robot.position.x}
            onChange={handleXChange}
          />
        </label>
        <br/>
        <label className="Form__label">
          y<input
            className="Form__input"
            type="number"
            value={robot.position.y}
            onChange={handleYChange}
          />
        </label>
      </fieldset>
    </form>
  );
};

export default Form;
