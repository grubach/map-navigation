import React from "react";
import { Stage, Sprite, Container } from "@inlet/react-pixi";
import useWindowSize from "./hooks/useWindowSize";
import { useTransform } from "./contexts/transform";
import { useRobot } from "./contexts/robot";
import useMouseScale from "./hooks/useMouseScale";
import useMouseMove from "./hooks/useMouseMove";
import useMouseRotate from "./hooks/useMouseRotate";
import useTouchMove from "./hooks/useTouchMove";
import useTouchScale from "./hooks/useTouchScale";
import useTouchRotate from "./hooks/useTouchRotate";
import image from "./assets/House-Plan-Example-BW-2000px.jpeg";
import robotImage from "./assets/robot.png";
import "./Plan.css";

const preventDefault = (e: React.MouseEvent) => e.preventDefault();

const Plan: React.FC = () => {
  const windowSize = useWindowSize();
  const [transform] = useTransform();
  const [robot] = useRobot();
  useMouseScale();
  useMouseMove();
  useMouseRotate();
  useTouchMove();
  useTouchScale();
  useTouchRotate();

  return (
    <Stage
      className="Plan"
      width={windowSize.width}
      height={windowSize.height}
      options={{ backgroundColor: 0xeeeeee }}
      onContextMenu={preventDefault}
    >
      <Container {...transform}>
        <Sprite image={image} anchor={0} />
        <Sprite
          image={robotImage}
          scale={0.1 / transform.scale}
          rotation={-transform.rotation}
          position={robot.position}
          anchor={0.5}
        />
      </Container>
    </Stage>
  );
};

export default Plan;
