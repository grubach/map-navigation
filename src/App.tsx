import React from "react";
import { TransformProvider } from "./contexts/transform";
import { RobotProvider } from "./contexts/robot";
import Plan from "./Plan";
import Form from "./Form";
import "./App.css";

const App: React.FC = () => (
  <TransformProvider>
    <RobotProvider>
      <div className="App">
        <Plan />
        <Form />
      </div>
    </RobotProvider>
  </TransformProvider>
);

export default App;
