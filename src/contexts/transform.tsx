import React from "react";

type Transform = {
  position: { x: number; y: number };
  scale: number;
  rotation: number;
};

const zeroTransform: Transform = {
  position: { x: 0, y: 0 },
  scale: 2,
  rotation: 0,
};

const TransformContext = React.createContext<
  [Transform, React.Dispatch<React.SetStateAction<Transform>>]
>([zeroTransform, () => {}]);

const TransformProvider: React.FC<{ initialTransform?: Transform }> = ({
  children,
  initialTransform = zeroTransform,
}) => {
  const [transform, setTransform] = React.useState<Transform>(initialTransform);

  return (
    <TransformContext.Provider value={[transform, setTransform]}>
      {children}
    </TransformContext.Provider>
  );
};

const useTransform = () => {
  const context = React.useContext(TransformContext);
  return context;
};

export { TransformProvider, useTransform };
