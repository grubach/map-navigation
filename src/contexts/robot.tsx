import React from "react";

type Robot = {
  position: { x: number; y: number };
};

const zeroRobot: Robot = {
  position: { x: 0, y: 0 },
};

const RobotContext = React.createContext<
  [Robot, React.Dispatch<React.SetStateAction<Robot>>]
>([zeroRobot, () => {}]);

const RobotProvider: React.FC<{ initialRobot?: Robot }> = ({
  children,
  initialRobot = zeroRobot,
}) => {
  const [transform, setRobot] = React.useState<Robot>(initialRobot);

  return (
    <RobotContext.Provider value={[transform, setRobot]}>
      {children}
    </RobotContext.Provider>
  );
};

const useRobot = () => {
  const context = React.useContext(RobotContext);
  return context;
};

export { RobotProvider, useRobot };
